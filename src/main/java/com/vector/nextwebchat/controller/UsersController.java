package com.vector.nextwebchat.controller;

import com.vector.nextwebchat.storage.UserStorage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Set;

@RestController
@RequestMapping(value= "/api")
@Api(value="API REST Next-Chat-Web")
@CrossOrigin(origins="*")

public class UsersController {

    @GetMapping("/registration/{userName}")
    @ApiOperation(value="Endpoint que garante a solicitação de registro do usuário e caso ele tente se registrar novamente ele gera uma exceção informando que o usuário já está logando.")
    public ResponseEntity<Void> register(@PathVariable String userName) {
        System.out.println("Handling register user request: " + userName);
        try {
            UserStorage.getInstance().setUser(userName);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/fetchAllUsers")
    @ApiOperation(value="Endpoint que lista todos os usuários que se registraram e estão na fila para atendimento.")
    public Set<String> fetchAll() {
        return UserStorage.getInstance().getUsers();
    }
}
