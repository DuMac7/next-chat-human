package com.vector.nextwebchat.atendimento.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class AtendimentoModel {
    /**
	 * 
	 * @param id
	 * @param nome
	 * @param numeroProtocolo
	 * @param telefone
	 * @param demandante
	 * @param canal
	 * @param userState
	 * @param naoLidas
	 * @param userId
	 * @param usuarioAtendente
	 * @param usuario
	 * @param dataCadastro
	 */

    public AtendimentoModel(String id, String nome, String numeroProtocolo, String telefone, String demandante, String canal,
    String userStat, String userId, String usuarioAtendente, String usuario, Timestamp dataCadastro) {
        super();
        this.id = id;
        this.nome = nome;
        this.numeroProtocolo = numeroProtocolo;
        this.telefone = telefone;
        this.demandante = demandante;
        this.canal = canal;
        this.userState = userState;
        this.userId = userId;
        this.usuarioAtendente = usuarioAtendente;
        this.usuario = usuario;
        this.dataCadastro = dataCadastro;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String id;

    @Column(nullable = false, length = 150)
    @NotEmpty(message = "{campo.nome.obrigatorio}")
    public String nome;

    @Id
    public String numeroProtocolo;
    
    public String telefone;
    
    public String demandante;
    
    public String canal;
    
    public String userState;
    
    public String userId;
    
    public String usuarioAtendente;
    
	public String usuario;
	
    private Timestamp dataCadastro;
    



}
